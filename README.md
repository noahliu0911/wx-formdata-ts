# wx-formdata-ts
> 只将项目由JS改为TS，项目[源地址](https://github.com/zlyboy/wx-formdata)，如原作者不允许本仓库改动行为请[邮件联系](mailto:lbpeter0911@163.com)删除

在小程序中使用formdata上传数据，可实现多文件上传

# 用法
跟浏览器中的FormData对象类似
引入ts文件
```js
import FormData from "./FromData"
```
new一个FormData对象
```js
let formData = new FormData();
```
调用它的[append()](#formdataappend)方法来添加字段或者调用[appendFile()](#formdataappendfile)方法添加文件
```js
formData.append("name", "value");
formData.appendFile("file", filepath, "文件名");
```
添加完成后调用它的[getData()](#formdatagetdata)生成上传数据，之后调用小程序的wx.request提交请求
```js
let data = formData.getData();
wx.request({
  url: 'https://接口地址',
  header: {
    'content-type': data.contentType
  },
  data: data.buffer,
});
```

## JS转FormData

```ts
/** 被选中的文件对象 */
interface FileData {
  /** 微信返回文件路径 */
  filePath: string,
  /** 要设定的文件名 */
  name: string
}

// 这是传递进请求方法的data对象，原本是要给 wx.request 转成 json的，
interface httpData {
    file?: FileData[],
    [ key: ( string | number ) ]: any;
}

function ObjToFormData(data:httpData){
  let formData = new FormData();
  for (const key in data) {
    if (Object.prototype.hasOwnProperty.call(data, key)) {
      const item = data[key];
      if(key === "file"){
        item.forEach( (item:FileData) => {
          formData.appendFile('file',item.filePath,item.name)
        })
      } else {
        formData.append(key,item)
      }
    }
  }

  return formData.getData();
}
```
经过上面的转换 就能够的得到最后的结果 给 wx.reqest 用了

# 成员函数
### FormData.append()
#### 语法
```js
formData.append(name, value);
```
#### 参数
| 参数名 | 描述 |
| :---------- | :-----------|
| name | value中包含的数据对应的表单名称 |
| value | 表单的值 |

### FormData.appendFile()
#### 语法
```js
formData.appendFile(name, filepath, fileName);
```
#### 参数
| 参数名 | 描述 |
| :---------- | :-----------|
| name | value中包含的数据对应的表单名称 |
| filepath | 文件路径 | 
| fileName | 文件名【可选】 | 

### FormData.getData()
#### 语法
```js
let data = formData.getData();
```
#### 返回值对象属性
| 属性名 | 描述 |
| :---------- | :-----------|
| buffer | 表单数据的ArrayBuffer对象 |
| contentType | http请求Content-Type头部内容 | 